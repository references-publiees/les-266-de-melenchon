# Les 266

Référencement et Sauvegarde des différentes sources d'informations sur les 266 "erreurs de bouton" de JEan-Luc Mélenchon au Parlement Européen.



### Les 266 "erreurs de boutons" de Jean Luc Mélenchon au Parlement Européen

* Source de l'article : https://www.liberation.fr/checknews/2019/06/19/est-ce-que-melenchon-s-est-trompe-266-fois-de-vote-lorsqu-il-etait-eurodepute_1734483
* Impressions écran de l'article du journal _Libération_ : 


![les 266](https://gitlab.com/references-publiees/les-266-de-melenchon/raw/master/documentation/images/impr-ecran/liberation/Firefox_Screenshot_2019-12-06T11-16-09.094Z.png?inline=false)